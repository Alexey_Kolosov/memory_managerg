/////////////////////////////////////////////////////////////////////
function Manager(template){
	var TMEMORY = template.TMEMORY;
	var VOLUME = this.VOLUME = template.VOLUME;
	var NULL_INDEX = this.NULL_INDEX = template.NULL_INDEX || (VOLUME-VOLUME);
	var UNIT_INDEX = this.UNIT_INDEX = template.UNIT_INDEX || 1;
	var BASE_VOLUME = this.BASE_VOLUME = template.BASE_VOLUME || VOLUME;
	var RATIO = template.RATIO || 4;
	var SCALE = template.SCALE || 2;
	var START_VOLUME = this.START_VOLUME = template.START_VOLUME || BASE_VOLUME;
	var LIST_1_INDEX = template.LIST_1_INDEX || NULL_INDEX;
	var LIST_2_INDEX = template.LIST_2_INDEX || (LIST_1_INDEX+UNIT_INDEX);
	var LIST_3_INDEX = template.LIST_3_INDEX || (LIST_2_INDEX+UNIT_INDEX);
	var LIST_4_INDEX = template.LIST_4_INDEX || (LIST_3_INDEX+UNIT_INDEX);
	var TREE_2_INDEX = template.TREE_2_INDEX || (LIST_4_INDEX+UNIT_INDEX);
	var TREE_3_INDEX = template.TREE_3_INDEX || (TREE_2_INDEX+UNIT_INDEX);
	var TREE_4_INDEX = template.TREE_4_INDEX || (TREE_3_INDEX+UNIT_INDEX);
	var TREE_5_INDEX = template.TREE_5_INDEX || (TREE_4_INDEX+UNIT_INDEX);

	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	var VOLUME_ONE = UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_TWO = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_THREE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FOUR = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FIVE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var ROOT_QUANTITY = this.ROOT_QUANTITY = VOLUME_FOUR+VOLUME_FOUR;

	this._ = { mem: new TMEMORY(), actualVolume: START_VOLUME };
	this.memory = new BaseMemory({VOLUME:VOLUME,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);

//	this.list1=new List({VOLUME:VOLUME,LIST_INDEX:LIST_1_INDEX,BASE_INDEX:VOLUME_ZERO,LAST_INDEX:VOLUME/VOLUME_FOUR,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
//	this.list2=new List({VOLUME:VOLUME,LIST_INDEX:LIST_2_INDEX,BASE_INDEX:VOLUME/VOLUME_FOUR,LAST_INDEX:VOLUME/VOLUME_TWO,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
//	this.list3=new List({VOLUME:VOLUME,LIST_INDEX:LIST_3_INDEX,BASE_INDEX:VOLUME/VOLUME_TWO,LAST_INDEX:VOLUME_THREE*(VOLUME/VOLUME_FOUR),NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
//	this.list4=new List({VOLUME:VOLUME,LIST_INDEX:LIST_4_INDEX,BASE_INDEX:VOLUME_THREE*(VOLUME/VOLUME_FOUR),LAST_INDEX:VOLUME,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);

	this.list1=new List({VOLUME:VOLUME,LIST_INDEX:LIST_1_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.list2=new List({VOLUME:VOLUME,LIST_INDEX:LIST_2_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.list3=new List({VOLUME:VOLUME,LIST_INDEX:LIST_3_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.list4=new List({VOLUME:VOLUME,LIST_INDEX:LIST_4_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.tree2=new BalancedTree({VOLUME:VOLUME,NODE_VOLUME:2,TREE_INDEX:TREE_2_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.tree3=new BalancedTree({VOLUME:VOLUME,NODE_VOLUME:3,TREE_INDEX:TREE_3_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.tree4=new BalancedTree({VOLUME:VOLUME,NODE_VOLUME:4,TREE_INDEX:TREE_4_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.tree5=new BalancedCartesianTree({VOLUME:VOLUME,NODE_VOLUME:5,TREE_INDEX:TREE_5_INDEX,NULL_INDEX:NULL_INDEX,UNIT_INDEX:UNIT_INDEX},this._);
	this.lastIndex;
	this.roots=new Array(ROOT_QUANTITY); 
	this.roots[0]=LIST_1_INDEX;
	this.roots[1]=LIST_2_INDEX;
	this.roots[2]=LIST_3_INDEX;
	this.roots[3]=LIST_4_INDEX;
	this.roots[4]=TREE_2_INDEX;
	this.roots[5]=TREE_3_INDEX;
	this.roots[6]=TREE_4_INDEX;
	this.roots[7]=TREE_5_INDEX;
	var i = ROOT_QUANTITY;
	var p = NULL_INDEX;
	var j; //unsigned (j>>>0)

	this.allocateFirst = function(volume,pointer) {  
		if (pointer == undefined) pointer = NULL_INDEX;
		var index;
		if (volume < VOLUME_ONE) {
			index = this.allocateInRoots(pointer);
		}
		else {
			var firstVolume = volume;
			var firstIndex = NULL_INDEX;

			if (firstVolume <= VOLUME_FOUR) {
				if (firstVolume <= VOLUME_THREE) {
					if (firstVolume <= VOLUME_TWO) {
						if (firstVolume == VOLUME_ONE)  { 
							if ((firstIndex = this.list1.first()) == NULL_INDEX) {
								if ((firstIndex = this.list2.first()) == NULL_INDEX) {
									if ((firstIndex = this.list3.first()) == NULL_INDEX) firstIndex = this.list4.first();
								}
							}
						} 
						index = this.tree2.first();
						if ((index != NULL_INDEX) && ((firstIndex == NULL_INDEX) || (firstIndex > index))) {
							firstVolume = VOLUME_TWO;
							firstIndex = index;
						}
					}
					index = this.tree3.first();
					if ((index != NULL_INDEX) && ((firstIndex == NULL_INDEX) || (firstIndex > index))) {
						firstVolume = VOLUME_THREE;
						firstIndex = index;
					}
				}
				index = this.tree4.first();
				if ((index != NULL_INDEX) && ((firstIndex == NULL_INDEX) || (firstIndex > index))) {
					firstVolume = VOLUME_FOUR;
					firstIndex = index;
				}
				index = this.tree5.first();
				if ((index != NULL_INDEX) && ((firstIndex == NULL_INDEX) || (firstIndex > index))) {
					firstVolume = VOLUME_FIVE;
					firstIndex = index;
				}
			}
			index = NULL_INDEX;

			if (firstVolume == VOLUME_ONE) {
				index = this.allocateInList(pointer);

				(index+volume > this.lastIndex)? (index == NULL_INDEX)? this.lastIndex: (this.lastIndex = index+volume-UNIT_INDEX): this.lastIndex;
			}
			else if(firstVolume<=VOLUME_FOUR){		
				index = this.allocateIn234Tree(volume,firstVolume,pointer);
				(index+volume > this.lastIndex)? (index == NULL_INDEX)? this.lastIndex: (this.lastIndex = index+volume-UNIT_INDEX): this.lastIndex;
			}
			if(index==NULL_INDEX){					
				index = this.allocateIn5TreeUnsafe(volume,pointer);
			}
		}
		return index;
	}
	this.allocate = this.allocateFirst;
/*	this.deallocate = function(pointer,volume) {
		if(volume == VOLUME_ZERO){
			return volume;
		}
//#ifdef LIST_TREE_DOUBLE_EXTRACT
	//	TINDEX pastIndex = NULL_INDEX;
	//	bool pastExtracted = false;
//#endif
		TINDEX pre = pointer-VOLUME_ONE;
		TINDEX past = pointer+volume;
		TINDEX index;
		TINDEX checked;
		TVOLUME vol;
		bool justResize = false;

		// before
		index = NULL_INDEX;
		checked = NULL_INDEX;
		vol = VOLUME_ZERO;
		if(pre!=NULL_INDEX&&this.memory.isIndex(pre)){
			TINDEX fcni = this.memory.cell(pre);

			if(this.memory.isIndex(fcni)) {
				if(pointer-fcni>VOLUME_THREE){
					if((checked = this.tree5.filterLast(pre,vol))!=NULL_INDEX){
//#ifdef AVL_OPTIMIZATION
//						index = this.tree5.tryToResize(checked,vol,volume,false,justResize);
//#else
						index = this.tree5.extract(checked,vol);
//#endif

					}
//					if(index==NULL_INDEX&&(checked = tree4.filterLast(pre,vol))!=NULL_INDEX){
					else if((checked = tree4.filterLast(pre,vol))!=NULL_INDEX){
						index = tree4.extract(checked);
					}
				}
//				if(index==NULL_INDEX&&(pointer-fcni == VOLUME_THREE)&&(checked = this.tree3.filterLast(pre,vol))!=NULL_INDEX){
				else if ((checked = this.tree3.filterLast(pre,vol))!=NULL_INDEX) {
					index = this.tree3.extract(checked);
				}
			}
			if(index==NULL_INDEX&&(checked = this.tree2.filterLast(pre,vol))!=NULL_INDEX){
				index = this.tree2.extract(checked);
			}
			if(index==NULL_INDEX 
//#ifdef LIST_TREE_FILTER
	//			&& (checked = this.listFilterLast(pre,vol))!=NULL_INDEX
//#endif
				){
//#ifdef LIST_TREE_DOUBLE_EXTRACT
//#ifdef LIST_TREE_FILTER
	//			if ((checked = this.listFilterFirst(past,vol)) != NULL_INDEX) 
//#endif
	//			{
	//				this.listDoubleExtract(index = pre,pastIndex = past);
	//				this.pastExtracted = true;
	//			}
//#ifdef LIST_TREE_FILTER
	//			else index = this.listExtract(pre);
//#endif
//#else
				index = this.listExtract(pre);
//#endif
				vol = VOLUME_ONE;
			}
			if(index!=NULL_INDEX){
				pointer = index;
				volume = volume + vol;
			}
		}

		// past
		index = NULL_INDEX;
		checked = NULL_INDEX;
		vol = VOLUME_ZERO;
		SET(tc,tb);
		if(past!=NULL_INDEX&&this.memory.isIndex(past)){
//#ifdef LIST_TREE_DOUBLE_EXTRACT
	//		if (pastIndex != NULL_INDEX) {
	//			index = pastIndex;
	//			vol = VOLUME_ONE; 
	//		}
//#endif
			if(index==NULL_INDEX) if ((checked = this.tree5.filterFirst(past,vol//,actualVolume()
											))!=NULL_INDEX){
				//index = this.tree5.extract(checked,vol);
//#ifdef AVL_OPTIMIZATION
	//			index = this.tree5.tryToResize(checked,vol,volume,true,justResize);
//#else
				index = this.tree5.extract(checked,vol);
//#endif
			}
//			if(index==NULL_INDEX&&(checked = this.tree4.filterFirst(past,vol))!=NULL_INDEX){
			else if((checked = this.tree4.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree4.extract(checked);
			}
			if(index==NULL_INDEX&&(checked = this.tree3.filterFirst(past,vol//,actualVolume()
											))!=NULL_INDEX){
				index = this.tree3.extract(checked);
			}
			if(index==NULL_INDEX&&(checked = this.tree2.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree2.extract(checked);
			}
			if(index==NULL_INDEX 
//#ifdef LIST_TREE_DOUBLE_EXTRACT
	//			&& !pastExtracted
//#endif
//#ifdef LIST_TREE_FILTER
	//			&& (checked = this.listFilterFirst(past,vol))!=NULL_INDEX
//#endif
				){
				index = this.listExtract(past);
				vol = VOLUME_ONE;
			}

			if(index!=NULL_INDEX){
				volume = volume + vol;
			}
		}


//#ifdef AVL_OPTIMIZATION
	//	if(!justResize){
	//		this.freeAddUnsafe(pointer,volume);
	//	}
//#else
		this.freeAddUnsafe(pointer,volume);
//#endif

		return volume;
	}
*/	this.deallocateUnsafe = function(volume, pointer) {           
		if(volume == VOLUME_ZERO){
			return volume;
		}
		var pre = pointer-VOLUME_ONE;
		var past = pointer+volume;
		var index;
		var checked;
		var vol = { volume: VOLUME_ZERO };
		var justResize = false;

		// before
		index = NULL_INDEX;
		checked = NULL_INDEX;
		if(pre!=NULL_INDEX&&this.memory.isIndex(pre)){
			var fcni = this.memory.cell(pre);

			if(pointer-fcni>VOLUME_THREE){
				if((checked = this.tree5.filterLast(pre,vol))!=NULL_INDEX){
					index = this.tree5.extract(checked,vol);

						
				}
				if(index==NULL_INDEX&&(checked = this.tree4.filterLast(pre,vol))!=NULL_INDEX){
					index = this.tree4.extract(checked);

				}
			}
			if(index==NULL_INDEX&&(pointer-fcni == VOLUME_THREE)&&(checked = this.tree3.filterLast(pre,vol))!=NULL_INDEX){
				index = this.tree3.extract(checked);
					
			}
		}
		if(index==NULL_INDEX&&(checked = this.tree2.filterLast(pre,vol))!=NULL_INDEX){
			index = this.tree2.extract(checked);
		}
		if(index==NULL_INDEX){
			index = this.listExtract(pre);
			vol.volume = VOLUME_ONE;
		}
		if(index!=NULL_INDEX){
			pointer = index;
			volume = volume + vol.volume;
		}

		// past
		index = NULL_INDEX;
		checked = NULL_INDEX;
		vol.volume = VOLUME_ZERO;
		if(past!=NULL_INDEX&&this.memory.isIndex(past)){
			if(index==NULL_INDEX&&(checked = this.tree5.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree5.extract(checked,vol);
			}
			if(index==NULL_INDEX&&(checked = this.tree4.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree4.extract(checked);
				
			}
			if(index==NULL_INDEX&&(checked = this.tree3.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree3.extract(checked);
				
			}
			if(index==NULL_INDEX&&(checked = this.tree2.filterFirst(past,vol))!=NULL_INDEX){
				index = this.tree2.extract(checked);
				
			}
			if(index==NULL_INDEX){
				index = this.listExtract(past);
				vol.volume = VOLUME_ONE;
			}
			if(index!=NULL_INDEX){
				volume = volume + vol.volume;
			}
		}
		this.freeAddUnsafe(pointer,volume);
		return volume;
	}
	this.deallocate = this.deallocateUnsafe;
	this.allocateInRoots=function(pointer){
		var index = NULL_INDEX;
		var roots=new Array(ROOT_QUANTITY); 
		this.roots[0]=LIST_1_INDEX;
		this.roots[1]=LIST_2_INDEX;
		this.roots[2]=LIST_3_INDEX;
		this.roots[3]=LIST_4_INDEX;
		this.roots[4]=TREE_2_INDEX;
		this.roots[5]=TREE_3_INDEX;
		this.roots[6]=TREE_4_INDEX;
		this.roots[7]=TREE_5_INDEX;
		var i=0;
		for(i-=i;i<ROOT_QUANTITY-1;i++){
			if((index = this.memory.cell(this.roots[i]))!=NULL_INDEX) {
				return index;
			}
		}
		index = this.memory.cell(this.roots[i]);
		return index;
	}	
	this.allocateInList=function(pointer) {  
		var index = NULL_INDEX;
		if(pointer!=NULL_INDEX){
			index = this.listExtract(pointer);
		}
		if(index==NULL_INDEX){
			index = this.list1.extractFirst();
		}else 
			return index;
		if(index==NULL_INDEX){
			index = this.list2.extractFirst();
		}else 
			return index;
		if(index==NULL_INDEX){
			index = this.list3.extractFirst();
		}else 
			return index;
		if(index==NULL_INDEX){
			index = this.list4.extractFirst();
		}
		return index;
	}; 
	this.allocateIn234Tree=function(volume, pointer){
		var index = NULL_INDEX;
		var tree=((volume<VOLUME_TWO)?VOLUME_TWO:volume);
		for(tree;tree<=VOLUME_FOUR;tree++){
			index = this.treeExtractFirst(tree);	
			if (index!=NULL_INDEX){	
				if(tree>volume){
					this.freeAddUnsafe(index+volume,tree-volume);// 5 impossible
				}
				break;
			}
		}
		return index;
	}
	this.allocateIn5TreeUnsafe=function(volume, pointer){
		var index = NULL_INDEX;
		var foundVolume = { foundVolume: VOLUME_ZERO };
		index = this.tree5.findPlace(volume,foundVolume);
		if (foundVolume.foundVolume>volume){
			this.freeAdd(index+volume,foundVolume.foundVolume-volume);
		}
		(index+volume > this.lastIndex)? (index == NULL_INDEX)? this.lastIndex: (this.lastIndex = index+volume-UNIT_INDEX): this.lastIndex;
		return index;
	}
	this.listExtract=function(index){
		var quarter = this.getMemoryQuarter(index);
		switch (quarter){
			case VOLUME_ONE:
				return this.list1.extract(index);
			case VOLUME_TWO:
				return this.list2.extract(index);
			case VOLUME_THREE:
				return this.list3.extract(index);
			case VOLUME_FOUR:
				return this.list4.extract(index);
		}
		return NULL_INDEX;
	}
	this.listAdd=function(index){
		var quarter = this.getMemoryQuarter(index);
		switch (quarter){
			case VOLUME_ONE:
				this.list1.add(index);
				break;
			case VOLUME_TWO:
				this.list2.add(index);
				break;
			case VOLUME_THREE:
				this.list3.add(index);
				break;
			case VOLUME_FOUR:
				this.list4.add(index);
				break;
		}
	}
	this.getMemoryQuarter=function(index){
		var quarter1 = ((((VOLUME/VOLUME_FOUR)>>>0)+(VOLUME-((VOLUME/VOLUME_FOUR)>>>0)*VOLUME_FOUR+VOLUME_THREE)/VOLUME_FOUR)>>>0);
		return VOLUME_ONE+((index/quarter1)>>>0);
	}
	this.freeAddUnsafe=function(index, nodeVolume){
		if(nodeVolume == VOLUME_ONE){
			this.listAdd(index);
		}else if(nodeVolume == VOLUME_TWO){
			this.tree2.add(index);
		}else if(nodeVolume == VOLUME_THREE){
			this.tree3.add(index);
		}else if(nodeVolume == VOLUME_FOUR){
			this.tree4.add(index);
		}else if(nodeVolume > VOLUME_FOUR){
			this.tree5.add(index,nodeVolume);
		}
	}
	this.freeAdd=this.freeAddUnsafe;
	this.treeExtractFirst=function(nodeVolume){
		if(nodeVolume == VOLUME_TWO){
			return this.tree2.extractFirst();
		}else if(nodeVolume == VOLUME_THREE){
			return this.tree3.extractFirst();
		}else if(nodeVolume == VOLUME_FOUR){
			return this.tree4.extractFirst();
		}else{
			return NULL_INDEX;
		}
	}
	this.reallocate=function(volume, ovolume, pointer) {
		if (volume > ovolume) {
			if (coallocate(pointer+ovolume,volume-ovolume) == NULL_INDEX) {
				deallocateUnsafe(ovolume,pointer);
				pointer = allocate(volume);
			}
		}
		else if (volume < ovolume)
			if (deallocateUnsafe(ovolume-volume,pointer+(ovolume-volume)) != (ovolume-volume)) pointer = NULL_INDEX;
		return pointer;
	};
	this.coallocate=function(volume,pointer) {
		return NULL_INDEX;
	};
	for (i -= i; i < ROOT_QUANTITY; i++)
		for (j = i; j < ROOT_QUANTITY; j++) 
			if (this.roots[i] > this.roots[j]) {
				p = this.roots[i];
				this.roots[i] = this.roots[j];
				this.roots[j] = p;
			}
		this.lastIndex = this.roots[--i];
		for (i -= i; i < ROOT_QUANTITY; i++) {
			this.freeAdd(p,this.roots[i]-p);
			p = this.roots[i]+UNIT_INDEX;
		}

}
///////////////////////
function BalancedCartesianTree(template,p){
	BaseTree.call(this,template,p);

	var VOLUME = this.memory.VOLUME;
	var NULL_INDEX = this.memory.NULL_INDEX;
	var UNIT_INDEX = this.memory.UNIT_INDEX;

	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	var VOLUME_ONE = UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_TWO = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_THREE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FOUR = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FIVE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;

	this.add=function(index, key2param)
	{
		this.addNodeI(index, key2param, this.root(), VOLUME_ZERO-VOLUME, VOLUME);	
	}
	this.first=function()
	{
		index=this.root();
		return this.memory.cell(index);
	}
	this.extract=function(index, findVolume){
		if (findVolume !== undefined) findVolume=this.memory.cell(index);
		index = this.findAndRemove(index, this.root(),VOLUME_ZERO-VOLUME, VOLUME);
		return index;
	}
	this.findPlace=function(volume, foundVolume){
		var proot = this.root();
		var current = this.memory.cell(proot);
 
		if(current != NULL_INDEX) if (this.memory.MVLM(current) >= volume)
		{
			var parent = this.memory.cell(proot);
			var leftRange = VOLUME_ZERO-VOLUME;
			var rightRange = VOLUME;
			var r = {};
			r.current = current;
			r.parent = parent;
			r.leftRange = leftRange;
			r.rightRange = rightRange; 
			this.findKey2(volume, r);
			current = r.current;
			parent = r.parent;
			leftRange = r.leftRange;
			rightRange = r.rightRange;
			var isLeaf;
//			var m = ""+this.memory.memory;
//			var d = "";
			if (
				(this.memory.NVLM(current)-volume > VOLUME_FOUR)
				&& ((this.memory.LBRI(current) == NULL_INDEX) ||
					(current+volume <= this.getCenter(leftRange,rightRange)+VOLUME_FOUR)) 
				&& ((rightRange == VOLUME) ||
					(this.memory.RBRI(current) != NULL_INDEX) ||					
					((this.memory.LBRI(parent) == current) && (this.getDistance(parent,rightRange) < this.getDistance(current+volume,rightRange)))) 
				) {
				if (this.memory.cell(proot) == current) {
					this.memory.cell(proot,undefined,current + volume);
				}
				isLeaf = this.memory.createTreeNode(current+(foundVolume.foundVolume=volume), this.memory.NVLM(current)-volume,this.memory.LBRI(current),this.memory.RBRI(current));
				this.memory.MVLM(isLeaf,this.calcKey2(isLeaf));
			}
			else {
				foundVolume.foundVolume = this.memory.NVLM(current);
				isLeaf = this.remove(proot, isLeaf = current, r, this.getCenter(leftRange,rightRange));
//				d = d+this.memory.memory;
				parent = r.parent;
			}
			if (parent != NULL_INDEX) this.recountKey2(this.memory.cell(proot), parent, isLeaf);
		}
		else current = NULL_INDEX;
		return current;
	}
	this.addNodeI=function(index, key2param, psubroot, leftRange, rightRange)
	{
		if(index==this.memory.cell(psubroot)) 
			return;
		var temp=this.memory.cell(psubroot);
		var currentNodeKey2param=key2param;
		while(temp != NULL_INDEX)
        	{
			var center = this.getCenter(leftRange,rightRange);
			if(this.getDistance(index,center) <= this.getDistance(temp,center) )
			{
				this.memory.createTreeNode(index,currentNodeKey2param,this.memory.LBRI(temp),this.memory.RBRI(temp));
				currentNodeKey2param = this.memory.NVLM(temp);   
				this.memory.MVLM(index,(this.memory.NVLM(index) > this.memory.MVLM(temp))? this.memory.NVLM(index) : this.memory.MVLM(temp));
				this.memory.cell(psubroot,undefined,index);
				if (index == temp) 
					return;
				if(temp < index)
				{
					psubroot = this.memory.LBRIndex(index);
					rightRange=center;
				}
				else
				{
					psubroot = this.memory.RBRIndex(index);
					leftRange=center;
				}
				index = temp;
			}
       			else
			{
				if(this.memory.MVLM(temp) < currentNodeKey2param)
        	        		this.memory.MVLM(temp,currentNodeKey2param);

				var less = (index < temp);
				var greater = (index > temp);

				psubroot = less? this.memory.LBRIndex(temp): greater? this.memory.RBRIndex(temp): psubroot;

				rightRange = less ? center: rightRange;
				leftRange = greater ? center: leftRange;
	        	}

			temp = this.memory.cell(psubroot);
	        }
	        this.memory.cell(psubroot,undefined,this.memory.createTreeNode(index, currentNodeKey2param));
	        this.memory.MVLM(index,currentNodeKey2param);
		return;
	}
	this.getDistance=function(index, center)
	{
		return (index>=center)?(index-center):(center-index);
	}
	this.getCenter=function(left, right)
	{
		return (left+((right-left)>>1));
	}
	this.getNearestRight = function(proot)
	{
		while (this.memory.LBRI(this.memory.cell(proot)) != NULL_INDEX) 
			proot = this.memory.LBRIndex(this.memory.cell(proot));	
		return proot;
	}
	
	this.getNearestLeft=function(proot)
	{
		while (this.memory.RBRI(this.memory.cell(proot)) != NULL_INDEX) 
			proot = this.memory.RBRIndex(this.memory.cell(proot));
		return proot;
	}
	this.find=function(index, r)
	{
        	var current = r.parent;
	        r.parent = NULL_INDEX;       
        	while(current != NULL_INDEX)
	        {
			var center = this.getCenter(r.leftRange,r.rightRange);
	 		if(current > index)
        		{// go to left
				r.rightRange = center;
				center = this.memory.LBRI(current);
        		        this.memory.LBRI(current,r.parent);
		                r.parent = current;
		                current = center;
			}
			else if (current < index)
			{// go to right
				r.leftRange = center;
				center = this.memory.RBRI(current);
                		this.memory.RBRI(current,r.parent);
				r.parent = current;
                		current = center;
			}
			else
				// if found
		        	return true;
                
		}
    
		// is't found
		return false;
	}
	this.findNoBreak=function(index, current)
	{
		while(current != NULL_INDEX)
		{
			if(current > index)
			{// go to left
				current = this.memory.LBRI(current);
			}
			else if (current < index)
			{// go to right
				current = this.memory.RBRI(current);
			}
			else break;
		}
		return current;
	}
	this.removeFromLeft=function(proot, refParent, refCurrent) 
	{
		var isRootDeleting = (refParent.parent == this.memory.cell(proot));
		var forReplaceLeft = this.memory.LBRI(refParent.parent);
		var forReplaceRight = this.memory.RBRI(refParent.parent);
		var right;

		while(refCurrent.current != NULL_INDEX)
		{
			right = this.memory.RBRI(refCurrent.current);
			
			if(right == NULL_INDEX)
			{
				refCurrent.current = this.memory.LBRI(refParent.parent = refCurrent.current);
		                this.memory.LBRI(refParent.parent,forReplaceLeft);
	        	        this.memory.RBRI(refParent.parent,forReplaceRight);
        	        	if(isRootDeleting)
		                {
					this.memory.cell(proot,undefined,refParent.parent);
					isRootDeleting = false;
		                }         
				right = refCurrent.current;
                
	        	        while((right != NULL_INDEX) && ((right = this.memory.RBRI(right)) == NULL_INDEX))
		                {
					right = this.memory.LBRI(refCurrent.current);
					this.memory.LBRI(refCurrent.current,refParent.parent);
					refParent.parent = refCurrent.current;
					refCurrent.current = right;        
	                	} 
		                if(right == NULL_INDEX)
				{
					if( (this.memory.LBRI(refParent.parent) != NULL_INDEX) &&
                			        (this.memory.LBRI(refParent.parent) < refParent.parent) && (this.memory.RBRI(refParent.parent) > refParent.parent) )
						{
							right = this.memory.RBRI(refParent.parent);
							this.memory.RBRI(refParent.parent,this.memory.LBRI(refParent.parent));
							this.memory.LBRI(refParent.parent,NULL_INDEX);						
						}
					return right;
				}
	        	        else
	        	        {
					forReplaceLeft = refParent.parent;
					forReplaceRight = NULL_INDEX;
				}
			}

			var pForReplace = this.memory.RBRIndex(refCurrent.current);

			do {
				this.memory.RBRI(refCurrent.current,refParent.parent);
				refParent.parent = refCurrent.current;
				refCurrent.current = right;
				right = this.memory.RBRI(refCurrent.current);
			} while(right != NULL_INDEX);

			right = this.memory.LBRI(refCurrent.current);//left

			this.memory.cell(pForReplace,undefined,refCurrent.current);

			if(isRootDeleting)
			{
				this.memory.cell(proot,undefined,refCurrent.current);
				isRootDeleting = false;
			}

			this.memory.LBRI(refCurrent.current,forReplaceLeft);
			this.memory.RBRI(refCurrent.current,forReplaceRight);
            
			refCurrent.current = right;//left
			forReplaceLeft = refParent.parent;
			forReplaceRight = NULL_INDEX;
		}
        
		if( (this.memory.LBRI(refParent.parent) != NULL_INDEX) && (this.memory.RBRI(refParent.parent) != NULL_INDEX) )
		{
			right = this.memory.LBRI(refParent.parent);//left
			this.memory.LBRI(refParent.parent,this.memory.RBRI(refParent.parent));
			this.memory.RBRI(refParent.parent,NULL_INDEX);
			return right;//left
		}
		else
			return NULL_INDEX;
	}
	this.removeFromRight=function(proot, refParent, refCurrent) 
	{
		var isRootDeleting = (refParent.parent == this.memory.cell(proot));
		var forReplaceLeft = this.memory.LBRI(refParent.parent);
		var forReplaceRight = this.memory.RBRI(refParent.parent);
		var left;

		while(refCurrent.current != NULL_INDEX)	
		{
			left = this.memory.LBRI(refCurrent.current);
            
			if(left == NULL_INDEX)
			{
				refCurrent.current = this.memory.RBRI(refParent.parent = refCurrent.current);
				this.memory.LBRI(refParent.parent,forReplaceLeft);
				this.memory.RBRI(refParent.parent,forReplaceRight);

				if(isRootDeleting)
				{
					this.memory.cell(proot,undefined,refParent.parent);
					isRootDeleting = false;
				}

				left = refCurrent.current;
                
				while((left != NULL_INDEX) && ((left = this.memory.LBRI(left)) == NULL_INDEX))
				{
					left = this.memory.RBRI(refCurrent.current);//right
					this.memory.RBRI(refCurrent.current,refParent.parent);
					refParent.parent = refCurrent.current;
					refCurrent.current = left;//right
				}
                
				if(left == NULL_INDEX)
				{
					if( (this.memory.LBRI(refParent.parent) != NULL_INDEX) &&
						(this.memory.LBRI(refParent.parent) < refParent.parent) && (this.memory.RBRI(refParent.parent) > refParent.parent) )
						{
							left = this.memory.LBRI(refParent.parent);
							this.memory.LBRI(refParent.parent,this.memory.RBRI(refParent.parent));
							this.memory.RBRI(refParent.parent,NULL_INDEX);						
						}
					return left;
				}
				else
				{
					forReplaceLeft = NULL_INDEX;
					forReplaceRight = refParent.parent;
				}
			}

			var pForReplace = this.memory.LBRIndex(refCurrent.current);

			do {
				this.memory.LBRI(refCurrent.current,refParent.parent);
				refParent.parent = refCurrent.current;
				refCurrent.current = left;
				left = this.memory.LBRI(refCurrent.current);
			} while(left != NULL_INDEX);
            
			left = this.memory.RBRI(refCurrent.current);//right
            
			this.memory.cell(pForReplace,undefined,refCurrent.current);

			if(isRootDeleting)
			{
				this.memory.cell(proot,undefined,refCurrent.current);
				isRootDeleting = false;
			}

			this.memory.LBRI(refCurrent.current,forReplaceLeft);
			this.memory.RBRI(refCurrent.current,forReplaceRight);
            
			refCurrent.current = left;
			forReplaceLeft = NULL_INDEX;
			forReplaceRight = refParent.parent;
	        }
        
	        if( (this.memory.LBRI(refParent.parent) != NULL_INDEX) && (this.memory.RBRI(refParent.parent) != NULL_INDEX) )
        	{
			left = this.memory.RBRI(refParent.parent);
       			this.memory.RBRI(refParent.parent,this.memory.LBRI(refParent.parent));
			this.memory.LBRI(refParent.parent,NULL_INDEX);
       			return left;
		}
		else
			return NULL_INDEX;
	}
	this.calcKey2=function(index)
	{
		return  (this.memory.LBRI(index) == NULL_INDEX)?
					((this.memory.RBRI(index) == NULL_INDEX)? 
						this.memory.NVLM(index) : 
						((this.memory.MVLM(this.memory.RBRI(index)) > this.memory.NVLM(index)? 
							this.memory.MVLM(this.memory.RBRI(index)): 
							this.memory.NVLM(index)))):
					(this.memory.RBRI(index) == NULL_INDEX)? 
						((this.memory.MVLM(this.memory.LBRI(index)) > this.memory.NVLM(index)? 
							this.memory.MVLM(this.memory.LBRI(index)): 
							this.memory.NVLM(index))):
					this.memory.MVLM(this.memory.LBRI(index)) > this.memory.MVLM(this.memory.RBRI(index)) ?
						(this.memory.MVLM(this.memory.LBRI(index)) > this.memory.NVLM(index) ?
							this.memory.MVLM(this.memory.LBRI(index)): this.memory.NVLM(index)) : 
						(this.memory.MVLM(this.memory.RBRI(index)) > this.memory.NVLM(index) ?
							this.memory.MVLM(this.memory.RBRI(index)): this.memory.NVLM(index));     
	}
	this.recountKey2=function(root, current, from)
	{        
		var key2 = (from == NULL_INDEX)? VOLUME_ZERO: this.memory.MVLM(from);
        
	        while(current != root)
        	{       
			var tmpCurrentNext;

			var isLeftBranch = 
				(this.memory.RBRI(current) == NULL_INDEX) || 
				(!(this.memory.LBRI(current) == NULL_INDEX) &&
				(!( (this.memory.RBRI(current)<current) ) &&
				(( (this.memory.LBRI(current)>current) ) || 
				(!(current < from)))));
			if(isLeftBranch)
			{
				tmpCurrentNext = this.memory.LBRI(current);
				this.memory.LBRI(current,from);
				from = this.memory.RBRI(current);
			}
			else
			{
				tmpCurrentNext = this.memory.RBRI(current);
				this.memory.RBRI(current,from);
				from = this.memory.LBRI(current);
			}
			key2 = ((from == NULL_INDEX)? key2: ((key2 < this.memory.MVLM(from))? this.memory.MVLM(from): key2));
			key2 = ((key2 < this.memory.NVLM(current))? this.memory.NVLM(current): key2);
            
			this.memory.MVLM(current,key2);
			from = current;
			current = tmpCurrentNext;            
		}
        
		if(from != NULL_INDEX)
			if(from < current)
				this.memory.LBRI(current,from);
			else
				this.memory.RBRI(current,from);

		this.memory.MVLM(current,this.calcKey2(current));
	}
	this.remove=function(proot, current, parent, center)
	{
		var from = NULL_INDEX;
		if(this.memory.LBRI(current) == NULL_INDEX) {
			if (this.memory.RBRI(current) == NULL_INDEX) 
			{
				if(parent.parent == NULL_INDEX)
					this.memory.cell(proot,undefined,NULL_INDEX);
				else
					if((this.memory.LBRI(parent.parent) != NULL_INDEX) && (this.memory.RBRI(parent.parent) != NULL_INDEX) && 
					(this.memory.LBRI(parent.parent) < parent.parent) && (this.memory.RBRI(parent.parent) > parent.parent))
						if(current < parent.parent)
						{
							from = this.memory.RBRI(parent.parent);
							this.memory.RBRI(parent.parent,this.memory.LBRI(parent.parent));
							this.memory.LBRI(parent.parent,NULL_INDEX);
						}
						else
						{
							from = this.memory.LBRI(parent.parent);
							this.memory.LBRI(parent.parent,this.memory.RBRI(parent.parent));
							this.memory.RBRI(parent.parent,NULL_INDEX);						
						}
			}
			else
			{// go to right
				var right = this.memory.RBRI(current);
				this.memory.RBRI(current,parent.parent);
				parent.parent = current;
				var r = {};
				r.current = right;
				from = this.removeFromRight(proot, parent, r);
				current = r.current;
			}
		}
		else if(this.memory.RBRI(current) == NULL_INDEX)
		{// go to left
			var left = this.memory.LBRI(current);
			this.memory.LBRI(current,parent.parent);
			parent.parent = current;
			var r = {};
			r.current = left;
			from = this.removeFromLeft(proot, parent, r);                
			current = r.current; 
		}
		else if(this.getDistance(center, this.memory.cell(this.getNearestLeft(this.memory.LBRIndex(current)))) < 
			this.getDistance(center, this.memory.cell(this.getNearestRight(this.memory.RBRIndex(current)))))
		{// go to left
			var left = this.memory.LBRI(current);
			this.memory.LBRI(current,parent.parent); 
			parent.parent = current;
			var r = {};
			r.current = left;
			from = this.removeFromLeft(proot, parent, r);
			current = r.current;
		}
		else
		{// go to right
			var right = this.memory.RBRI(current);
			this.memory.RBRI(current,parent.parent); 
			parent.parent = current;
			var r = {};
			r.current = right;
			from = this.removeFromRight(proot, parent, r);
			current = r.current;
		}
		return from;    
	}
	this.findAndRemove=function(index, proot, leftRange, rightRange, onlyFind)
	{
		if (onlyFind === undefined) onlyFind=false;
		var current = index;
		var parent = this.memory.cell(proot);
		var found = this.findNoBreak(index, parent);
		if(!onlyFind && found != NULL_INDEX)
		{
			var r = {};
			r.parent = parent;
			r.leftRange = leftRange;
			r.rightRange = rightRange;
			this.find(index, r);
			var isLeaf = this.remove(proot, current, r, this.getCenter(r.leftRange,r.rightRange));
			if (r.parent != NULL_INDEX) this.recountKey2(this.memory.cell(proot), r.parent, isLeaf);
		}
		return found;
	}
	this.findKey2=function(key2, r)
	{
		r.current = r.parent;
		r.parent = NULL_INDEX; 
		for(;this.memory.MVLM(r.current) >= key2;) 
		{
			var next = this.memory.LBRI(r.current);
			var center = this.getCenter(r.leftRange,r.rightRange);
			if((next != NULL_INDEX) && (this.memory.MVLM(next) >= key2))
			{
				r.rightRange = center;
				if (r.rightRange == NULL_INDEX) {
					alert("error!!!")
				}
				this.memory.LBRI(r.current,r.parent);
				r.parent = r.current;
				r.current = next;//left
			}
			else
			{
				if (this.memory.NVLM(r.current) >= key2) 
					return true;
				else
				{
					next = this.memory.RBRI(r.current);
					if(next != NULL_INDEX) 
					{
						r.leftRange = center;
						this.memory.RBRI(r.current,r.parent);
						r.parent = r.current;
						r.current = next;
        				}
					else break;
		 		}
			}
			continue;
		}
		return false;
	}	
}
/////////////////////////////////////////////////////////////////
function BalancedTree(template,p) {
	BaseTree.call(this,template,p);

	var VOLUME = this.memory.VOLUME;
	var NULL_INDEX = this.memory.NULL_INDEX;
	var UNIT_INDEX = this.memory.UNIT_INDEX;

	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	var VOLUME_ONE = UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_TWO = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_THREE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FOUR = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FIVE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;

	this.add = function(index) {
		this.addNodeI(index, this.root(), VOLUME_ZERO-VOLUME, VOLUME);
	}
	this.first = function(){
		var index = this.root();
		return this.memory.cell(index);
	}
	this.extractI = function(index, proot, leftRange, rightRange, onlyFind, findVolume){
		if (onlyFind === undefined) onlyFind=false;
		if (findVolume === undefined) findVolume=VOLUME_ZERO;
		var extracted = NULL_INDEX;
		while (this.memory.cell(proot) != NULL_INDEX){
			var center = this.getCenter(leftRange,rightRange);
			if (this.memory.cell(proot) > index){	// go to left subtree
				proot = this.memory.LBRIndex(this.memory.cell(proot));   //// ?????? ? ?? ? ? ? ? ?
				rightRange = center;
			}else if (this.memory.cell(proot) < index){ // go to right subtree
				proot = this.memory.RBRIndex(this.memory.cell(proot));
				leftRange = center;
			}else{	// node found!
				extracted = index;
				if(onlyFind){
					break;
				}
				if (this.memory.RBRI(this.memory.cell(proot)) == NULL_INDEX){
					if (this.memory.LBRI(this.memory.cell(proot)) == NULL_INDEX){
						this.memory.cell(proot,undefined,NULL_INDEX);
						break;
					}
					this.memory.cell(proot,undefined,this.removeLI(this.memory.cell(proot),this.memory.LBRIndex(this.memory.cell(proot))));
				}else if (this.memory.LBRI(this.memory.cell(proot)) == NULL_INDEX){
					this.memory.cell(proot,undefined,this.removeRI(this.memory.cell(proot),this.memory.RBRIndex(this.memory.cell(proot))));
				}else{
					var left = this.getNearestLeft(this.memory.LBRIndex(this.memory.cell(proot)));
					var right = this.getNearestRight(this.memory.RBRIndex(this.memory.cell(proot)));
					if(this.getDistance(left,center)<this.getDistance(right,center)){
						this.memory.cell(proot,undefined,this.removeLI(this.memory.cell(proot),left));
					}else{
						this.memory.cell(proot,undefined,this.removeRI(this.memory.cell(proot),right)); 
					}
				}
				break;
			}
		}
		return extracted;
	}
	this.extract = function(index,onlyFind,findVolume){
		if (onlyFind === undefined) onlyFind=false;
		if (findVolume === undefined) findVolume=VOLUME_ZERO;
		return this.extractI(index, this.root(),VOLUME_ZERO-VOLUME,VOLUME, onlyFind, findVolume);
	}
	this.extractFirst = function(p){
		if (p === undefined) p = this.root();
		var r = this.memory.cell(p);	
		if (r != NULL_INDEX) for(;true;r = this.memory.cell(p = this.memory.RBRIndex(this.memory.cell(p)))) {
			for (;this.memory.LBRI(r) != NULL_INDEX;r = this.memory.cell(p = this.memory.LBRIndex(this.memory.cell(p))));
			if (this.memory.RBRI(r) == NULL_INDEX) break;
		}
		this.memory.cell(p,undefined,NULL_INDEX);
		return r;
	}
	this.addNodeI=function(index, psubroot, leftRange, rightRange) {
		if(index==this.memory.cell(psubroot)) 
			return;
		var temp;
		while((temp=this.memory.cell(psubroot))!=NULL_INDEX){
			var center = this.getCenter(leftRange,rightRange);
			if(this.getDistance(index,center)<=this.getDistance(temp,center)){
				this.memory.createTreeNode(index,this.memory.LBRI(temp),this.memory.RBRI(temp));
				this.memory.cell(psubroot,undefined,index);
				if(temp < index){
        	        		psubroot = this.memory.LBRIndex(index);
                			rightRange=center;
				}else{
                			psubroot = this.memory.RBRIndex(index);
	                		leftRange=center;
				}
				index = temp;
				if (index == this.memory.cell(psubroot)) 
					return;
			}else{

				var less = (index < temp), greater = (index > temp);               
				psubroot = less? this.memory.LBRIndex(temp): greater? this.memory.RBRIndex(temp): psubroot;
				rightRange = less? center: rightRange;
				leftRange = greater? center: leftRange;
			}
		}
        	this.memory.cell(psubroot,undefined,this.memory.createTreeNode(index));
		return;
	}
	this.getDistance=function(index, center){
		return (index>=center)?(index-center):(center-index);
	}
	this.getCenter=function(left, right){
		return (left+((right-left)>>1));
	}

	this.removeLI=function(root, pcurr){
        	var pc = undefined;
		var oroot, lbri_oroot = NULL_INDEX, rbri_root = this.memory.RBRI(root), res = NULL_INDEX;
		var skip = true;
		while (this.memory.cell(pcurr)!=NULL_INDEX){
			if (this.memory.RBRI(this.memory.cell(pcurr))==NULL_INDEX){
				var temp = this.memory.RBRI(this.memory.cell(pcurr));
				this.memory.cell(this.memory.RBRIndex(this.memory.cell(pcurr)),undefined,rbri_root);
				rbri_root = temp;
				if (pc !== undefined) {
					this.memory.cell(pc,undefined,this.memory.cell(pcurr));
					temp = this.memory.LBRI(oroot);
					this.memory.cell(this.memory.LBRIndex(oroot),undefined,skip? temp: lbri_oroot);
					skip = (temp==root);
				}
				else res = this.memory.cell(pcurr);
				pc = pcurr;
				oroot = root;
				lbri_oroot = temp;
				root = this.memory.cell(pcurr);
				pcurr = this.memory.LBRIndex(root);
			}else{
				pcurr = this.memory.RBRIndex(this.memory.cell(pcurr));
			}
		}
		if (pc !== undefined) {
			this.memory.cell(pc,undefined,NULL_INDEX);
			if(this.memory.LBRI(oroot)!=root){
				this.memory.cell(this.memory.LBRIndex(root),undefined,this.memory.LBRI(oroot));
			}
			this.memory.cell(this.memory.LBRIndex(oroot),undefined,skip? this.memory.LBRI(oroot): lbri_oroot);
		}
		return res;
	}

	this.removeRI=function(root, pcurr){
		var pc = undefined;
		var oroot, lbri_root = this.memory.LBRI(root), rbri_oroot = NULL_INDEX, res = NULL_INDEX;
		var skip = true;
		while (this.memory.cell(pcurr)!=NULL_INDEX){
			if (this.memory.LBRI(this.memory.cell(pcurr))==NULL_INDEX){
				var temp = this.memory.LBRI(this.memory.cell(pcurr));
				this.memory.cell(this.memory.LBRIndex(this.memory.cell(pcurr)),undefined,lbri_root);
				lbri_root = temp;
				if (pc !== undefined) {
					this.memory.cell(pc,undefined,this.memory.cell(pcurr));
					temp = this.memory.RBRI(oroot);
					this.memory.cell(this.memory.RBRIndex(oroot),undefined,skip? temp: rbri_oroot);
					skip = (temp==root);
				}
				else res = this.memory.cell(pcurr);
				pc = pcurr;
				oroot = root;
				rbri_oroot = temp;
				root = this.memory.cell(pcurr);
				pcurr = this.memory.RBRIndex(root);
			}else{
				pcurr = this.memory.LBRIndex(this.memory.cell(pcurr));
			}
		}
		if (pc !== undefined) {
			this.memory.cell(pc,undefined,NULL_INDEX);
			if(this.memory.RBRI(oroot)!=root){
				this.memory.cell(this.memory.RBRIndex(root),undefined,this.memory.RBRI(oroot));
			}
			this.memory.cell(this.memory.RBRIndex(oroot),undefined,skip? this.memory.RBRI(oroot): rbri_oroot);
		}
		return res;
	}	
	this.getNearestRight = function(p)
	{
		while (this.memory.LBRI(this.memory.cell(p)) != NULL_INDEX) 
			p = this.memory.LBRIndex(this.memory.cell(p));	
		return p;
	}
	
	this.getNearestLeft=function(p)
	{
		while (this.memory.RBRI(this.memory.cell(p)) != NULL_INDEX) 
			p = this.memory.RBRIndex(this.memory.cell(p));
		return p;
	}
}
////////////////////////////////////////////////////////////////
function BaseTree(template,p) {	
	this.memory = new NodesMemory(template,p);
	this.ROOT_INDEX = template.TREE_INDEX;
	this.root = function(i) {
		if (i !== undefined) this.ROOT_INDEX=i;
		return this.ROOT_INDEX;
	}
	this.filterFirst = function(index,volume){
		return this.memory.filterFirst(index,volume);
	}
	this.filterLast = function(index,volume){
		return this.memory.filterLast(index,volume);
	}
}
////////////////////////////
function ListTree(template,p){
	this.memory = new BaseMemory(template,p);
	var VOLUME = this.memory.VOLUME;
	var TREE_INDEX = this.TREE_INDEX = template.TREE_INDEX;
	var BASE_INDEX = this.BASE_INDEX = template.BASE_INDEX;
	var LAST_INDEX = this.LAST_INDEX = template.LAST_INDEX;
	var LENGHT = this.LENGHT = (template.LENGHT || (LAST_INDEX - BASE_INDEX));
	var NULL_INDEX = this.memory.NULL_INDEX;
	var UNIT_INDEX = this.memory.UNIT_INDEX;

	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	var VOLUME_ONE = UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_TWO = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_THREE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;

	this.memory.cell(this.ROOT_INDEX = TREE_INDEX,undefined,NULL_INDEX);
	this.root = function(i){
		if (i !== undefined) this.ROOT_INDEX=i;
		return this.ROOT_INDEX;
	}
	this.filterFirst = function(index,volume) {
	}
	this.filterLast = function(index,volume) {
		return this.filterFirst(index,volume);
	}
	this.getRelativeRoot = function() {
		var root = this.memory.cell(this.root());
		root = (root == BASE_INDEX)? this.memory.cell(root): root;
		return this.toRelative(root);
	}
	this.setAbsoluteRoot = function(value) {
		var proot = this.root();
		this.memory.cell(((this.memory.cell(proot) == BASE_INDEX)? this.memory.cell(proot): proot),undefined,value);
	}
	this.setRelativeRoot = function() {
		this.setAbsoluteRoot(this.toAbsolute(value));
	}
	this.add = function(index) {
	}
	this.first = function() {
	}
	this.extract = function(index) {
	}
	this.extractFirst = function() {
	}
	this.extractFirst = function(root,parent,type) {
	}
	this.initConstants = function() {
		var MAXTCELL = ~VOLUME_ZERO;
		var TCELLBITS = VOLUME_ZERO;
		while (MAXTCELL > VOLUME_ZERO) {
			TCELLBITS = TCELL_BITS + VOLUME_ONE;
			MAXTCELL = (MAXTCELL>>VOLUME_ONE);
		}
		var BYTE_SIZE = this.BYTE_SIZE = (VOLUME_ONE<<VOLUME_THREE);
		var sizeofTCELL = TCELLBITS/BYTE_SIZE;
		var sizeofTINDEX = this.sizeofTINDEX = sizeofTCELL;
		var sizeofTVOLUME = this.sizeofTVOLUME = sizeofTCELL;/// javascript specials

		var vol = LENGHT;//-VOLUME_ONE;
		var need_size = VOLUME_ZERO;
		while(vol != VOLUME_ZERO){
			vol = vol/VOLUME_TWO;
			need_size=need_size+VOLUME_ONE;
		}
//#ifdef MAX_SHIFT
		var size = (sizeofTINDEX<<VOLUME_THREE);
		this.SHIFT = size - need_size;
		this.SHIFT = (this.SHIFT>need_size)?need_size:this.SHIFT;
//#else
//		this.SHIFT = VOLUME_TWO;
//#endif
		var shift = this.SHIFT;
		this.MASK = VOLUME_ZERO;
		this.MAIN_MASK = VOLUME_ZERO;
		while(shift != VOLUME_ZERO){
			shift = shift - VOLUME_ONE;
			this.MASK = (this.MASK+this.MASK)+VOLUME_ONE;		//0000...0011
			this.MAIN_MASK = (this.MAIN_MASK+this.MAIN_MASK);
		}
		shift = sizeofTVOLUME*BYTE_SIZE-this.SHIFT;
		while(shift != need_size){
			shift = shift - VOLUME_ONE;
			this.MASK = (this.MASK+this.MASK);
			this.MAIN_MASK = (this.MAIN_MASK+this.MAIN_MASK);
		}
		this.EXT_MASK = this.MASK;
		while(shift != VOLUME_ZERO){
			shift = shift - VOLUME_ONE;
			this.EXT_MASK = (this.EXT_MASK+this.EXT_MASK);				//1100...0000
			this.MAIN_MASK = (this.MAIN_MASK+this.MAIN_MASK)+VOLUME_ONE;
		}
		var nsize = need_size+this.SHIFT-VOLUME_ONE;	
		this.NODE_SIZE = nsize/this.SHIFT;
		this.REL_NULL_INDEX = this.toRelative(NULL_INDEX);
	}
	this.addNodeI = function(index,root,parent,type,leftRange,rightRange) {
	}
	this.extractI = function(index,root,parent,type,leftRange,rightRange) {
	}
	this.getCenter = function(left,right) {
		return (left+((right-left)>>1));
	}
	this.removeLI = function(root,parent,type,center){
	}
	this.removeRI = function(root,parent,type,center){
	}
	this.replaceLI = function(root,parent,type,center){
	}
	this.replaceRI = function(root,parent,type,center){
	}
	this.getNearestRight = function(_root,_parent,_type,center){
	}
	this.getNearestLeft = function(_root,_parent,_type,center){
	}
	this.getDistance = function(index,center){
	}
	this.getChild = function(parent,type){
	}
	this.setChild = function(parent,type,value){
	}
	this.toAbsolute = function(index) {
		return (index==VOLUME_ZERO)?NULL_INDEX:(index+BASE_INDEX-this.BASE_OFFSET);
	}
	this.toRelative = function(index) {
		return (index==NULL_INDEX)?VOLUME_ZERO:(index-BASE_INDEX+this.BASE_OFFSET);
	}
	this.getMain = function(index) {
		return this.memory.cell(index)&this.MAIN_MASK;
	}
	this.setMain = function(index,main) {
		this.memory.cell(index,undefined,(this.memory.cell(index)&this.EXT_MASK)|(main&this.MAIN_MASK));
	}
	this.getExt = function(index) {
		return this.memory.cell(index)&this.EXT_MASK;
	}
	this.setExt = function(index,ext) {
		this.memory.cell(index) = ((ext&this.EXT_MASK)|(this.memory.cell(index)&this.MAIN_MASK));
	}
	this.getExtPart = function(ext_full,partN) {
		return (ext_full<<(sizeofTINDEX*this.BYTE_SIZE-(this.NODE_SIZE-partN)*this.SHIFT))&this.EXT_MASK;
	}
	this.addExtPart = function(index) {
	}
	this.getShiftedExt = function(index) {
		return getExt(index)>>(sizeofTINDEX*this.BYTE_SIZE-this.SHIFT);
	}
	this.getChildren = function(root,_lbri,_lbri_cell,_rbri,_prev,_index,center,lookNearest) {
	}
	this.getChildren = function(root,_lbri,_lbri_cell,_rbri,_prev1,_prev2,_index1,_index2,center1,center2,lookNearest) {
	}
	this.replaceOldNew = function(root,parent,type,prev_old,__old,__new,lbri_cell){
	}
	this.replace = function(root,parent,type,prev_old,__old,__new,lbri_cell){
	}
	this.remove = function(parent,root,type,index,next,prev,lbri) {
	}
	this.remove = function(type,index,next,prev,lbri) {
	}
	this.scrollExt = function(extPart,next,lbri) {
	}
	this.place = function(index,next,p,extPart,parent,type,t) {
	}
	this.place = function(index,next,p,extPart,parent,type,t,lbri) {
	}
	this.place = function(index,lbri_cell,extPart) {
	}
}
////////////////////////////
function List(template,p){
	this.memory = new BaseMemory(template,p);
	var NULL_INDEX = this.memory.NULL_INDEX;
	var LIST_INDEX = template.LIST_INDEX;
	this.memory.cell(this.ROOT_INDEX = LIST_INDEX,undefined,NULL_INDEX);
	this.root = function(i){
		if (i !== undefined) this.ROOT_INDEX=i;
		return this.ROOT_INDEX;
	}
	this.add = function(index){
		var list = this.root();
		while ((this.memory.cell(list)!=NULL_INDEX)&&(this.memory.cell(list)<=index)){
			list=this.memory.cell(list);
		}
		this.memory.cell(index,undefined,this.memory.cell(list));
		this.memory.cell(list,undefined,index);
	}	
	this.first = function(){
		return this.memory.cell(this.root());	
	}
	this.extract = function(index){
		var list = this.root();	
		while(this.memory.cell(list)!=NULL_INDEX){
			if(this.memory.cell(list)==index){
				this.memory.cell(list,undefined,this.memory.cell(index));
				return index;
			}
			list=this.memory.cell(list);
		}
		return NULL_INDEX;
	}
	this.extractFirst = function(){
		var first = this.memory.cell(this.root());
		if(first != NULL_INDEX){
			this.memory.cell(this.root(),undefined,this.memory.cell(first));
		}
		return first;
	}
}
////////////////////////////////////////////////////////
function NodesMemory(template,p) {
	BaseMemory.call(this,template,p);
	var NULL_INDEX = this.NULL_INDEX;
	var UNIT_INDEX = this.UNIT_INDEX;
	var NODE_VOLUME = template.NODE_VOLUME;

	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	var VOLUME_ONE = UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_TWO = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_THREE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FOUR = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;
	var VOLUME_FIVE = UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX+UNIT_INDEX-UNIT_INDEX;

	if (NODE_VOLUME == 2) {
		this.LBRI=function(index, v){
		        return this.cell(index,undefined,v);///correct
	        }
		this.LBRIndex=function(index){
		        return index;///correct
	        }
		this.RBRI=function(index,v){
	        	return this.cell(index,VOLUME_ONE,v);
		}
		this.RBRIndex=function(index){
		        return index+VOLUME_ONE;
	        }
		this.FCNI=function(index){
			return index;
		}
		this.NVLM=function(index){
			return VOLUME_TWO;
		}
		this.createTreeNode=function(index,l,r){
			if (l === undefined) l=NULL_INDEX;
			if (r === undefined) r=NULL_INDEX;
			this.LBRI(index,l);
			this.RBRI(index,r);
			return index;
		}
		this.filter=function(index){
			return (((this.LBRI(index) < index) || (this.LBRI(index) == NULL_INDEX)) &&
				((index < this.RBRI(index)) || (this.RBRI(index) == NULL_INDEX)));
		}
		this.filterFirst=function(index,volume){
			return this.filter(index)? ((volume.volume = VOLUME_TWO) == VOLUME_TWO)? index: index: NULL_INDEX;
		}
		this.filterLast=function(index,volume){
			return this.filterFirst(index-VOLUME_ONE,volume);
		}
	}
	else if (NODE_VOLUME == 3) { 
		this.LBRI=function(index, v){
			return this.cell(index,undefined,v);
		}
		this.LBRIndex=function(index){
		        return index;
	        }
		this.RBRI=function(index,v){
			return this.cell(index,VOLUME_ONE,v);
		}
		this.RBRIndex=function(index){
		        return index+VOLUME_ONE;
	        }
		this.FCNI=function(index,v){
			return this.cell(index,VOLUME_TWO,v);
		}
	 	this.NVLM=function(index){
			return VOLUME_THREE;
		}
		this.createTreeNode=function(index,l,r){
			if (l === undefined) l=NULL_INDEX;
			if (r === undefined) r=NULL_INDEX;
			this.LBRI(index,l);
			this.RBRI(index,r);
			this.FCNI(index,index);
			return index;

		}
		this.filter=function(index){
			return ((this.FCNI(index) == index) && ((this.LBRI(index) < index) || (this.LBRI(index) == NULL_INDEX)));
		}
		this.filterFirst=function(index,volume){
			return ((VOLUME_THREE<=this.p.actualVolume-index))? ((volume.volume = VOLUME_THREE) == VOLUME_TWO)? index: index: NULL_INDEX;
		}
		this.filterLast=function(index,volume){
			return (index-this.cell(index)==VOLUME_TWO)? this.filterFirst(this.cell(index),volume): NULL_INDEX;
		}
	}
	else if (NODE_VOLUME == 4) {
		this.LBRI=function(index,v){
			return this.cell(index,VOLUME_ONE,v);
		}
		this.LBRIndex=function(index){
		        return index+VOLUME_ONE;
	        }
		this.RBRI=function(index,v){
			return this.cell(index,VOLUME_TWO,v);
		}
		this.RBRIndex=function(index){
		        return index+VOLUME_TWO;
	        }
		this.FCNI=function(index,v){
			return this.cell(index,VOLUME_THREE,v);
		}
	 	this.NVLM=function(index,v){
			return this.cell(index,undefined,v);
		}
		this.createTreeNode=function(index,l,r){
			if (l === undefined) l=NULL_INDEX;
			if (r === undefined) r=NULL_INDEX;
			this.RBRI(index,r);
			this.LBRI(index,l);
			this.NVLM(index,VOLUME_FOUR);
			this.FCNI(index,index);
			return index;
		}
		this.filter=function(index){
			return (this.NVLM(index) == VOLUME_FOUR);
		}
		this.filterFirst=function(index,volume){
			return ((this.NVLM(index)==VOLUME_FOUR) &&(VOLUME_FOUR<=this.p.actualVolume-index)
			&& (this.FCNI(index)==index) && this.filter(index))? ((volume.volume = VOLUME_FOUR) == VOLUME_TWO)? index: index: NULL_INDEX;
		}
		this.filterLast=function(index,volume){
			return (index-this.cell(index))==VOLUME_THREE? this.filterFirst(this.cell(index),volume): NULL_INDEX;
		} 	 
	}  
	else if (NODE_VOLUME == 5) { 
		this.LBRI=function(index,v){
			return this.cell(index,VOLUME_ONE,v);
		}
		this.LBRIndex=function(index){
		        return index+VOLUME_ONE;
	        }
		this.RBRI=function(index,v){
			return this.cell(index,VOLUME_TWO,v);
		}
		this.RBRIndex=function(index){
		        return index+VOLUME_TWO;
	        }
		this.NVLM=function(index,v){
			return this.cell(index,undefined,v);
		}
		this.FCNI=function(index,v){
			return this.cell(index,this.NVLM(index)-VOLUME_ONE,v);
		}
		this.MVLM=function(index,v){
			return this.cell(index,VOLUME_THREE,v);
		}
		this.createTreeNode=function(index,nodeVolume,l,r){
			if (l === undefined) l=NULL_INDEX;
			if (r === undefined) r=NULL_INDEX;
			this.RBRI(index,r);
			this.LBRI(index,l);
			this.NVLM(index,nodeVolume);
			this.FCNI(index,index);
			this.MVLM(index,nodeVolume);
			return index;
		}
		this.filter=function(index,valid){
			return ((valid || (this.NVLM(index) <= this.p.actualVolume-index) && (this.FCNI(index) == index))
			&& ((this.LBRI(index) != this.RBRI(index)) || (this.LBRI(index) == NULL_INDEX) || (this.RBRI(index) == NULL_INDEX)));
		}
		this.filterFirst=function(index,volume){
			var nvlm = this.cell(index);
			if((nvlm>VOLUME_FOUR) && (nvlm<=this.p.actualVolume-index) && (this.cell(index,nvlm-VOLUME_ONE)==index) && this.filter(index,true)){
				volume.volume = nvlm;
				return index;
		        }
			else{
				return NULL_INDEX;
		        }
		}
		this.filterLast=function(index,volume){
			var fcni = this.cell(index);
			if((index > fcni) && (index-fcni>VOLUME_THREE) && (this.cell(fcni)+fcni-VOLUME_ONE==index)){
				volume.volume = this.cell(fcni);
				return fcni;
			}
			else{
				return NULL_INDEX;
			}
		} 
	}
}
////////////////////////////////////////////////////////
function BaseMemory(template,p) {
	var VOLUME = this.VOLUME = template.VOLUME;
	var NULL_INDEX = this.NULL_INDEX = template.NULL_INDEX;
	var UNIT_INDEX = this.UNIT_INDEX = template.UNIT_INDEX;
	var VOLUME_ZERO = UNIT_INDEX-UNIT_INDEX;
	this.p = p;
	this.memory = p.mem;
	this.isIndex = function(index){
		return (index >= VOLUME_ZERO) && ((this.p.actualVolume === undefined) || (index < this.p.actualVolume));
	}
	this.cell = function(index, o, v) {
		var offset = o || VOLUME_ZERO;
		if (v != undefined) this.memory[index+offset] = v;
		if (index == NULL_INDEX && v*4 > VOLUME) {
			alert("error!");
		}
		return this.memory[index+offset];
	}
	this.indexToPointer = function(index){
		return index;
	}
	this.pointerToIndex = function(pointer){
		return pointer;
	}
////////////////////////////////////////////////////
}



